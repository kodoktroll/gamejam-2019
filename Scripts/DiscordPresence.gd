extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var discord_state = {
    "game_id": "640017746758860800",
    "game_state": "Sedang main"
}
var DiscordStuff = load("res://gdnative/libdiscord.gdns").new()

# Called when the node enters the scene tree for the first time.
func _ready():
    DiscordStuff.start(discord_state["game_id"])
    DiscordStuff.state = discord_state["game_state"]
    DiscordStuff.update()
    pass # Replace with function body.

func changeStatus(state_dict, status):
    var new_state_dict = state_dict
    new_state_dict["game_state"] = status
    return new_state_dict

func setStatus(status):
    discord_state = changeStatus(discord_state, status)
    DiscordStuff.state = discord_state["game_state"]
    DiscordStuff.update()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
