extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var game_timer = Timer.new()
var game_time_limit = 60
var lose_screen = "Game Over"

# Called when the node enters the scene tree for the first time.
func _ready():
	game_timer = Timer.new()
	pass # Replace with function body.

func setup():
	game_timer = Timer.new()
	game_timer.set_wait_time(game_time_limit)

func start():
	game_timer.start()
	
func get_game_timer():
	return game_timer
	
func stop():
	game_timer.stop()


func time_left():
	return game_timer.get_time_left()



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
