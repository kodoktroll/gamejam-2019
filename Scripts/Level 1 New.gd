extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var lose_screen = "Game Over"

# Called when the node enters the scene tree for the first time.
func handleLoseScreen():
	get_tree().change_scene(str("res://Scenes/" + lose_screen + ".tscn"))

func _ready():
	GameTime.setup()
	var game_timer = GameTime.get_game_timer()
	add_child(game_timer)
	game_timer.connect("timeout", self, "_on_GameTimer_timeout")
	GameTime.start()
	pass # Replace with function body.

func _on_GameTimer_timeout():
	handleLoseScreen()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
