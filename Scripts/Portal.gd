extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var enter_portal_name = self.get_name()
export (String) var exit_portal_name
export (bool) var is_win_portal

var win_screen = "Win Screen New"
onready var animator = self.get_node("Animator")
onready var timer = getTimer()
onready var global_timer = getGlobalTimer()
var timer_delay = 2


# Called when the node enters the scene tree for the first time.
# func _ready():
# 	pass # Replace with function body.

func _process(delta):
	animator.play("Portal Idle")
	
func getTimer():
	var node = get_owner().get_node("PortalTimeoutTimer")
	return node
	
func getGlobalTimer():
	var node = get_owner().get_node("Timer")
	return node

func getExitPortal(exit):
	var nodes = get_owner().get_children()
	for node in nodes:
		if node.get_name() == exit:
			return node

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Portal_body_entered(body):
	if is_win_portal:
		print("hehe")
		handleWinPortal()
		return
	else:
		var exit_portal = getExitPortal(exit_portal_name)
		if timer.is_stopped():
			moveBody(body, exit_portal)
	# pass # Replace with function body.

func moveBody(body, portal):
	body.position = portal.position
	timer.set_one_shot(true)
	timer.set_wait_time(timer_delay)
	timer.start()

func handleWinPortal():
	get_tree().change_scene(str("res://Scenes/" + win_screen + ".tscn"))
	GameTime.game_timer = global_timer

	pass # Replace with function body.
func on_PortalTimeoutTimer_timeout():
	timer.stop()

func _on_PortalTimeoutTimer_timeout():
	pass # Replace with function body.
